/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figury;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

class Kwadrat extends Figura {

        public Kwadrat(Graphics2D buf, int del, int w, int h) {
            super(buf, del, w, h);
 
       
        shape= new Rectangle(0,0, 20, 20);
        aft = new AffineTransform();                                  
        area = new Area(shape);
       
        }
}
